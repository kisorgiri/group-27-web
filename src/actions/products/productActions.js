export const ProductActionTypes = {
    SET_IS_LOADING: 'SET_IS_LOADING',
    PRODUCTS_RECEIVED: 'PRODUCTS_RECEIVED'
}

// functions that will dispatch a action

function getSomething(data) {
    return function (dispatch) {
        // dispatch is callback placeholder which is called to dispatcha afunction
    }
}
export const fetchProducts_ac = (params) => (dispatch) => {
    console.log('at actions from react component');
    dispatch({
        type: ProductActionTypes.SET_IS_LOADING,
        payload: true
    })

    setTimeout(() => {
        dispatch({
            type: ProductActionTypes.SET_IS_LOADING,
            payload: false
        })
    }, 5000)

}