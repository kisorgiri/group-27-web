import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notification from '../../../util/notification';
import { Link } from 'react-router-dom';
import { Loader } from '../../common/Loader/loader.component';
import { formatDate, formatTime } from '../../../util/dateProcessing';
import { connect } from 'react-redux'
import { fetchProducts_ac } from './../../../actions/products/productActions';

const IMG_URL = process.env.REACT_APP_IMG_URL;
class ViewProductComponent extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            products: []
        };
    }

    componentDidMount() {
        console.log('chek props >>', this.props)
        // this.setState({
        //     isLoading: true
        // })
        // httpClient
        //     .GET('/product', true)
        //     .then(response => {
        //         this.setState({
        //             products: response.data
        //         })
        //     })
        //     .catch(err => {
        //         notification.handleError(err);
        //     })
        //     .finally(() => {
        //         this.setState({
        //             isLoading: false
        //         })
        //     })
        this.props.getData();

    }

    removeProduct = (id, i) => {
        // ask confirmation before deletion
        let confirmation = window.confirm('Are you sure to remove?');
        if (confirmation) {
            httpClient.DELETE(`/product/${id}`, true)
                .then(response => {
                    notification.showInfo("Product Removed");
                    const { products } = this.state;
                    products.splice(i, 1);
                    this.setState({
                        products
                    })
                })
                .catch(err => {
                    notification.handleError(err);
                })
        }

    }

    editProduct = (id) => {
        this.props.history.push(`edit_product/${id}`)
    }
    render() {
        let content = this.props.isLoadingFromStore
            ? <Loader></Loader>
            : <table className="table">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        {/* <th>Color</th> */}
                        {/* <th>Price</th> */}

                        <th>Created Date</th>
                        <th>Created Time</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.state.products.map((item, index) => (
                            <tr key={item._id}>
                                <td>{index + 1}</td>
                                <td>
                                    <Link to={`/product_details/${item._id}`}>
                                        {item.name}
                                    </Link>
                                </td>
                                <td>{item.category}</td>
                                {/* <td>{item.color}</td> */}
                                {/* <td>{item.price}</td> */}
                                <td>{formatDate(item.createdAt, 'ddd YYYY/MM/DD')}</td>
                                <td>{formatTime(item.createdAt)}</td>
                                <td>
                                    <img src={`${IMG_URL}/${item.images[0]}`} width="200px" alt="product_image.jpg"></img>
                                </td>
                                <td>
                                    <button onClick={() => this.editProduct(item._id)} className="btn btn-info">edit</button>
                                    <button onClick={() => { this.removeProduct(item._id, index) }} className="btn btn-danger">delete</button>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        return (
            <>
                <h2>View Products</h2>
                {content}
            </>
        )
    }
}
// once component is connected to redux store
// we will have everything in props
// first object of connect method is always incoming props to component
// 2nd object of connect method is always ougoing props from component

// incoming props
const mapSateToProps = (store) => ({
    isLoadingFromStore: store.product.isLoading,
    b: store.product.products,
    c: 'd',
    e: 'hello'
})

// props but outgoing
const mapDispatchToProps = {
    getData: fetchProducts_ac
}

export const ViewProduct = connect(mapSateToProps, mapDispatchToProps)(ViewProductComponent)
