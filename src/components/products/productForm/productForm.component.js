import React, { Component } from 'react'
import { Button } from '../../common/Button/Button.component';
const IMG_URL = process.env.REACT_APP_IMG_URL;
const defaultForm = {
    name: '',
    description: '',
    brand: '',
    category: '',
    price: '',
    color: '',
    modelNo: '',
    manuDate: '',
    expiryDate: '',
    tags: '',
    offers: '',
    size: '',
    discountedItem: '',
    discountType: '',
    discountValue: ''
}
export class ProductForm extends Component {
    constructor() {
        super();
        this.state = {
            data: { ...defaultForm },
            error: { ...defaultForm },
            filesToUpload: [],
            isValidForm: false
        };
    }
    componentDidMount() {
        if (this.props.product) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...this.props.product,
                    discountedItem: this.props.product.discount ? this.props.product.discount.discountedItem : '',
                    discountType: this.props.product.discount ? this.props.product.discount.discountType : '',
                    discountValue: this.props.product.discount ? this.props.product.discount.discountValue : '',

                }
            })
        }
    }

    handelSubmit = e => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload)
    }

    handleChanage = e => {
        let { name, value, type, checked, files } = e.target;
        if (type === 'checkbox') {
            value = checked;
        }
        if (type === 'file') {
            return this.setState({
                filesToUpload: files
            })
        }

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*'
                break;
            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }
    render() {
        let previousImage = (this.props.product
            && this.props.product.images
            && this.props.product.images[0])
            ? <>
                <label>Previous Image</label>
                <br />
                <img src={`${IMG_URL}/${this.props.product.images[0]}`} alt="image.png" width="400px"></img>
            </>
            : ''

        let dicountContent = this.state.data.discountedItem
            ? <>
                <label>Discount Type</label>
                <select className="form-control" value={this.state.data.discountType} name="discountType" onChange={this.handleChanage}>
                    <option value="">(Slect Discount Type)</option>
                    <option value="percentage">Percentage</option>
                    <option value="quantity">Quantity</option>
                    <option value="value">Value</option>
                </select>
                <label>Discount Value</label>
                <input className="form-control" type="text" value={this.state.data.discountValue} name="discountValue" placeholder="Discount Value" onChange={this.handleChanage}></input>
            </>
            : ''
        return (
            <div>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handelSubmit}>
                    <label>Name</label>
                    <input className="form-control" type="text" value={this.state.data.name} name="name" placeholder="Name" onChange={this.handleChanage}></input>
                    <label>Description</label>
                    <input className="form-control" type="text" value={this.state.data.description} name="description" placeholder="Description" onChange={this.handleChanage}></input>
                    <label>Category</label>
                    <input className="form-control" type="text" value={this.state.data.category} name="category" placeholder="Category" onChange={this.handleChanage}></input>
                    <label>Brand</label>
                    <input className="form-control" type="text" value={this.state.data.brand} name="brand" placeholder="Brand" onChange={this.handleChanage}></input>
                    <label>Price</label>
                    <input className="form-control" type="text" value={this.state.data.price} name="price" placeholder="Price" onChange={this.handleChanage}></input>
                    <label>modelNo</label>
                    <input className="form-control" type="text" value={this.state.data.modelNo} name="modelNo" placeholder="modelNo" onChange={this.handleChanage}></input>
                    <label>Size</label>
                    <input className="form-control" type="text" value={this.state.data.size} name="size" placeholder="Size" onChange={this.handleChanage}></input>
                    <label>Tags</label>
                    <input className="form-control" type="text" value={this.state.data.tags} name="tags" placeholder="Tags" onChange={this.handleChanage}></input>
                    <label>Offers</label>
                    <input className="form-control" type="text" value={this.state.data.offers} name="offers" placeholder="Offers" onChange={this.handleChanage}></input>
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChanage}></input>
                    <label>Discounted Item</label>
                    <br></br>
                    {dicountContent}
                    <label>Manu Date</label>
                    <input className="form-control" type="date" value={this.state.data.manuDate} name="manuDate" onChange={this.handleChanage}></input>
                    <label>Expiry Date</label>
                    <input className="form-control" type="date" value={this.state.data.expiryDate} name="expiryDate" onChange={this.handleChanage}></input>
                    {previousImage}
                    <br />
                    <label>Image</label>
                    <input type="file" className="form-control" onChange={this.handleChanage}></input>
                    <br></br>

                    <Button
                        isSubmitting={this.props.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    ></Button>
                </form>
            </div>
        )
    }
}

