import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notification from '../../../util/notification';
import { Loader } from '../../common/Loader/loader.component';
import { ProductForm } from '../productForm/productForm.component';
const BaseURL = process.env.REACT_APP_BASE_URL;


export class EditProduct extends Component {
    constructor() {
        super();
        this.state = {
            product: {},
            isSubmitting: false,
            isLoading: false
        };
    }

    componentDidMount() {
        const productId = this.props.match.params['id'];
        this.setState({
            isLoading: true
        })
        httpClient.GET(`/product/${productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }
    edit = (data, files) => {
        // API call
        this.setState({
            isSubmitting: true
        })
        httpClient.UPLOAD(`${BaseURL}/product/${data._id}`, data, files, 'PUT')
            .then(response => {
                this.props.history.push('/view_product');
                notification.showInfo("Product Updated Successfully");
            })
            .catch(err => {
                notification.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <ProductForm
                product={this.state.product}
                title="Edit Product"
                isSubmitting={this.state.isSubmitting}
                submitCallback={this.edit}
            ></ProductForm>
        return content;
    }
}
