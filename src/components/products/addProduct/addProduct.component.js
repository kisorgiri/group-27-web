import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notification from '../../../util/notification';
import { ProductForm } from '../productForm/productForm.component';
const BaseURL = process.env.REACT_APP_BASE_URL;


export class AddProduct extends Component {

    constructor() {
        super();
        this.state = {
            isSubmitting: false
        };
    }
    add = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        httpClient
            .UPLOAD(`${BaseURL}/product`, data, files, 'POST')
            .then(response => {
                this.props.history.push('/view_product');
                notification.showInfo("Product added successfully")
            })
            .catch(err => {
                this.setState({
                    isSubmitting: false
                })
                notification.handleError(err);
            })
    }

    render() {
        return (
            <ProductForm title="Add Product" isSubmitting={this.state.isSubmitting} submitCallback={this.add}></ProductForm>
        )
    }
}
