import React from 'react';
import AppRouting from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { Provider } from 'react-redux';
import { store } from './../store';
// provider is a wrapper to wrapped react application with redux
// we have connect redux with react


export const App = () => {
    return (
        <>
            <Provider store={store}>
                <ToastContainer />
                <AppRouting />
            </Provider>

        </>


    )
}

//this file will supply the conent to be rendered

// component is basic buildig block of react
// component can be class based ===> stateful
// component can be functional ===> stateless 

// component can be stateless
// component can be statefull

// 