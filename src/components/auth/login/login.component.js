import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Button } from '../../common/Button/Button.component';
import notify from '../../../util/notification';
import httpClient from '../../../util/httpClient';

export class Login extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                username: '',
                password: ''
            },
            error: {
                username: '',
                password: ''
            },
            remember_me: false,
            isSubmitting: false,
            isValidForm: false,
        }
    }

    // init
    componentDidMount() {

    }

    // update
    componentDidUpdate(prevProps, prevState) {
        // this method is invoked whenever a props or state is changed
        // console.log('at update  prevstate >', prevState)
        // console.log('at update  current state >', this.state)
    }

    // destroy
    componentWillUnmount() {
    }


    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm(fieldName) {
        let errMsg = this.state.data[fieldName]
            ? ''
            : `${fieldName} is required*`
        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            let errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })

    }

    handleSubmit = (e) => {
        e.preventDefault();
        httpClient
            .POST(`/auth/login`, this.state.data)
            .then(response => {
                notify.showSuccess(`Welcome ${response.data.user.username}`)
                // localStorage
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('user', JSON.stringify(response.data.user));
                this.props.history.push("home");
            })
            .catch(err => {
                notify.handleError(err);
            })
    }

    render() {
        // UI logic remains inside render but not inside return
        return (
            <div>
                <h2>Login</h2>
                <p>Please login to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" id="username" placeholder="Username" name="username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" id="password" placeholder="Password" name="password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                        enabledLabel="Login"
                        disabledLabel="LogingIn..."
                    ></Button>
                </form>
                <p>Don't have an account?</p>
                <p>register <Link to="/register">here</Link></p>
            </div>

        )
    }
}

// state is an object to hold data within a componnent
// render method is mandatory in class based component
// render method must return single html node

// each and eveytime our state or props is cahnged render method called