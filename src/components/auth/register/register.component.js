import React, { Component } from 'react';
import { Button } from './../../common/Button/Button.component'
import { Link } from 'react-router-dom';
import notify from './../../../util/notification';
import httpClient from './../../../util/httpClient';

const defaultForm = {
    name: '',
    address: '',
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    phoneNumber: '',
    date_of_birth: '',
    gender: ''
}
export class Register extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        };
    }
    componentDidMount() {
        console.log('props >>', this.props);
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        });
    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*';
                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'Weak Password'
                    : 'required field*';
                break;
            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName] === this.state.data['password']
                        ? ''
                        : 'Password did not match'
                    : 'required field*';
                break;
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'Invalid Email'
                    : 'required field*'
                break;
            default:
                break;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            let errors = Object
                .values(this.state.error)
                .filter(item => item);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        // todo API call and it will take time
        httpClient
            .POST(`/auth/register`, this.state.data, { a: 'b' })
            .then(response => {
                notify.showInfo("Registration successfull please login")
                this.props.history.push('/'); // redirect
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <div>
                <h2>Register</h2>
                <p>Please Register to continue</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input type="text" className="form-control" placeholder="Name" name="name" onChange={this.handleChange}></input>
                    <label>Address</label>
                    <input type="text" className="form-control" placeholder="Address" name="address" onChange={this.handleChange}></input>
                    <label>Username</label>
                    <input type="text" className="form-control" placeholder="Username" name="username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Password" name="password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <label>Confirm Password</label>
                    <input type="password" className="form-control" placeholder="Confirm Password" name="confirmPassword" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.confirmPassword}</p>

                    <label>Email</label>
                    <input type="text" className="form-control" placeholder="Email Address" name="email" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>

                    <label>Gender</label>
                    <input type="text" className="form-control" placeholder="Gender" name="gender" onChange={this.handleChange}></input>
                    <label>Phone Number</label>
                    <input type="number" className="form-control" placeholder="Phone Number" name="phoneNumber" onChange={this.handleChange}></input>
                    <label>Date Of Birth</label>
                    <input type="date" className="form-control" name="date_of_birth" onChange={this.handleChange}></input>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    />
                    <p>Already Registered?</p>
                    <p>
                        Back to <Link to="/">login</Link>
                    </p>

                </form>
            </div>
        )
    }
}