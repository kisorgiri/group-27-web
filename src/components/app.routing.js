import React from 'react';
import { Login } from './auth/login/login.component';
import { Register } from './auth/register/register.component';
import { NavBar } from './common/header/header.component';
import { SideBar } from './common/sidebar/sidebar.component'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { AddProduct } from './products/addProduct/addProduct.component';
import { ViewProduct } from './products/viewProduct/viewProduct.component';
import { EditProduct } from './products/editProduct/editProduct.component';
import { Chat } from './common/chat/chat.component';

function NotFound() {
    return (
        <div>
            <p>Not Found</p>
            <img src="/images/found.png" alt="notfound.jpg" width="600px" />
        </div>
    )
}

function Dashboard(props) {
    console.log('localstorage >', localStorage.getItem('token'))
    console.log('localstorage >', JSON.parse(localStorage.getItem('user')))
    return (
        <p>Please use Side Navigation menu or contact system administrator for support</p>
    )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => (
            localStorage.getItem('token')
                ?
                <>
                    <div className="nav_bar">
                        <NavBar isLoggedIn={true} />
                    </div>
                    <div>
                        <SideBar></SideBar>
                    </div>
                    <div className="main_content">
                        <Component {...routeProps} />
                    </div>
                </>
                : <Redirect to="/"></Redirect>
        )
        } ></Route >
    )
}
const PublicRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => (

            <>
                <div className="nav_bar">
                    <NavBar isLoggedIn={localStorage.getItem('token') ? true : false} />
                </div>
                <div className="main_content">
                    <Component {...routeProps} />
                </div>
            </>
        )
        } ></Route >
    )
}
function AppRouting() {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute exact path="/" component={Login}></PublicRoute>
                <PublicRoute path="/register" component={Register}></PublicRoute>
                <ProtectedRoute path="/home" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_product" component={ViewProduct}></ProtectedRoute>
                <ProtectedRoute path="/chat" component={Chat}></ProtectedRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>
        </BrowserRouter>)
}

// API call
// protected route




export default AppRouting;


// summarize
// React-router-dom library
// BrowserRouter==> wrapper to wrap all routing configuration
// Route==> configuration of path and component
//  exact attribute inside Route
// Route will supply three props to components 
// History ==> function call
// match ==> params
// location ==> serch query
// Switch==> ensure only one route config is used
// Link==> used to navigate on user's click
