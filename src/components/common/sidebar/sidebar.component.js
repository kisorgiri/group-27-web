import React from 'react';
import { Link } from 'react-router-dom';
import './sidebar.component.css'

const SideNav = (props) => (
    <div className="sidenav">
        <Link to="/home">Home</Link>
        <Link to="/add_product">Add Product</Link>
        <Link to="/view_product">View Product</Link>
        <Link to="/search_product">Search Product</Link>
        <Link to="/chat">Messages</Link>
    </div>
)

export const SideBar = SideNav;