import React, { Component } from 'react'
import * as io from 'socket.io-client';
import { formatTime } from '../../../util/dateProcessing';
import notify from '../../../util/notification';
import './chat.component.css'



const socketURL = process.env.REACT_APP_SOCKET_URL;
const defaultData = {
    message: '',
    senderName: '',
    senderId: '',
    receiverName: '',
    receiverId: '',
    time: ''
}

export class Chat extends Component {
    constructor() {
        super()

        this.state = {
            msgBody: {
                ...defaultData
            },
            messages: [],
            users: []
        }
    }
    componentDidMount() {
        this.socket = io(socketURL);
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.runSocket();
    }
    runSocket() {

        this.socket.emit('new-user', this.currentUser.username);
        this.socket.on('reply-msg', (data) => {

            const { messages } = this.state;
            messages.push(data);
            this.setState(pre => ({
                messages,
                msgBody: {
                    ...pre.msgBody,
                    receiverId: data.senderId
                }
            }))
        })
        this.socket.on('reply-msg-own', (data) => {
            const { messages } = this.state;
            messages.push(data);
            this.setState(pre => ({
                messages
            }))
        })

        this.socket.on('users', (users) => {
            this.setState({
                users
            })
        })
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                [name]: value
            }
        }))
    }

    selectUser = (user) => {
        this.setState(pre => ({
            msgBody: {
                ...pre.msgBody,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }

    send = (e) => {
        e.preventDefault();
        const { msgBody, users } = this.state;
        if (!msgBody.receiverId) {
            return notify.showInfo("Please select a user to continue");
        }
        msgBody.time = Date.now();
        msgBody.senderName = this.currentUser.username;

        // find id of sender
        let user = users.find(user => user.name === this.currentUser.username)
        msgBody.senderId = user.id;

        this.socket.emit('new-msg', msgBody);
        this.setState(pre => ({
            msgBody: {
                ...pre.msgBody,
                message: ''
            }
        }))

    }

    render() {
        return (
            <>
                <h2>Let's Chat</h2>
                <div className="row">
                    <div className="col-md-6">
                        <ins>Messages</ins>
                        <div className="chatbox">
                            {this.state.messages.map((msg, i) => (
                                <div key={i}>
                                    <h3>{msg.senderName}</h3>
                                    <p>{msg.message}</p>
                                    <small> {formatTime(msg.time)}</small>
                                </div>
                            ))}
                        </div>
                        <form onSubmit={this.send} className="form-group row">
                            <input type="text" placeholder="your message here..." value={this.state.msgBody.message} name="message" className="form-control col-md-6" onChange={this.handleChange}></input>
                            <button className="btn btn-success col-md-2" type="submit">send</button>
                        </form>
                    </div>
                    <div className="col-md-6">
                        <ins>Users</ins>
                        <div className="chatbox">
                            {this.state.users.map((user, i) => (
                                <p key={i}>
                                    <button className="btn btn-default" onClick={() => this.selectUser(user)}>{user.name}</button>
                                </p>
                            ))}

                        </div>
                    </div>
                </div>

            </>
        )
    }
}
