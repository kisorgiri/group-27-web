import React from 'react';
import './header.component.css'
import { Link, withRouter } from 'react-router-dom';

const logout = (props) => {
    localStorage.clear();
    // navigate to login
    props.history.push('/');
}

function Header(props) {
    // props is incoming data from component
    const currentUser = JSON.parse(localStorage.getItem('user'));
    let content = props.isLoggedIn
        ? <ul className="nav_list">
            <li className="nav_item">
                <Link to="/home">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/profile">Profile</Link>
            </li>
            <li className="logout">
                {currentUser && currentUser.username} <button onClick={() => logout(props)} className="btn btn-info">Logout</button>
            </li>
        </ul>
        : <ul className="nav_list">
            <li className="nav_item">
                <Link to="/home">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/">Login</Link>
            </li>
            <li className="nav_item">
                <Link to="/register">Register</Link>
            </li>
        </ul>
    return content


}
export const NavBar = withRouter(Header);