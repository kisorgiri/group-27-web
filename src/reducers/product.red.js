import { ProductActionTypes } from './../actions/products/productActions';
const initialState = {
    products: [],
    isLoading: []
}
export const productReducer = (state = initialState, action) => {
    console.log('at reducers >>', action);
    // action will have data and task to perform
    switch (action.type) {
        case ProductActionTypes.SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        default:
            return {
                ...state
            }
    }
}