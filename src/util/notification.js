import { toast } from 'react-toastify';

function showSuccess(msg) {
    toast.success(msg);
}
function showInfo(msg) {
    toast.info(msg);
}
function showWarnings(msg) {
    toast.warn(msg);
}
function handleError(msg) {
    debugger;
    let error = msg.response;
    let errMsg = 'Something went wrong!';
    if (error && error.data) {
        errMsg = error.data.msg;
    }
    toast.error(errMsg);
    // step 1 take error message
    // step 2 parse the error
    // step 3 prepare error message
    // step 4 show the proper error message in UI

}

export default {
    showSuccess,
    showInfo,
    showWarnings,
    handleError
}