import axios from 'axios';

const BaseURL = process.env.REACT_APP_BASE_URL;
// crate base instance

function getHeaders(secured) {
    let options = {
        "Content-Type": "application/json"
    }
    if (secured) {
        options['Authorization'] = localStorage.getItem('token')
    }
    return options;
}
const http = axios.create({
    baseURL: BaseURL,
    responseType: 'json'
})

/**
 * http GET request call
 * @param {string} url 
 * @param {boolean} isSecure 
 * @param {object} params 
 */
function GET(url, isSecure = false, params = {}) {
    return http
        .get(url, {
            headers: getHeaders(isSecure),
            params
        });
}

function POST(url, data, isSecure = false, params = {}) {
    return http
        .post(url, data, {
            headers: getHeaders(isSecure),
            params
        });

}

function PUT(url, data, isSecure = false, params = {}) {
    return http
        .put(url, data, {
            headers: getHeaders(isSecure),
            params
        });

}

function DELETE(url, isSecure = false, params = {}) {
    return http
        .delete(url, {
            headers: getHeaders(isSecure),
            params
        });

}
function UPLOAD(url, data = {}, files = [], method = 'POST') {
    return new Promise(function (resolve, reject) {
        // file upload from react to server
        const xhr = new XMLHttpRequest();
        const formData = new FormData();

        // add file data in formdata
        if (files.length) {
            formData.append('image', files[0], files[0].name)
        }
        // add textual data in formdata
        for (let key in data) {
            formData.append(key, data[key]);
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.response)
                }
            }
        }

        xhr.open(method, `${url}?token=${localStorage.getItem('token')}`, true);
        xhr.send(formData);
    })

}

export default {
    GET,
    POST,
    PUT,
    DELETE,
    UPLOAD
}