import moment from 'moment';

export const formatTime = (date, format = 'hh:mm a') => {
    return moment(date).format(format);
}

export const formatDate = (date, format = 'ddd YYYY-MM-DD') => {
    return moment(date).format(format);
}