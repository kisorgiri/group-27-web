// redux
// redux is state management tool


// architecure
// MVC vs FLUX

// MVC ==> data flow is bidierection controller <==> model

// FLUX ==> unidirectional data flow 
// component is flux
// store ==> container where application data are kept
// dispatcher ==> is dispatch certain action to change store
// actions==> task
// view ==> react component(presentation layer)

// actions can be triggered from UI, and API response
// actions are sent to dispatcher and then it is sent to store
// once store is modified the data is reflected in UI


// FLUX vs REDUX
// flux can maintain multiple store
// store maintain logic to change store

// redux maintains single store(centralized store || single source of truth)
// reducer hold logic to change store

// redux architecture
// Views ,Actions, Reducers ,Store


// libraries to work with react
// redux ==> state management library 
// react-redux ===> glue to connect between react application and redux store
// middleware(thunk, saga) ==> delayed dispatch 
// redux-thunk
